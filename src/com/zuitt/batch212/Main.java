package com.zuitt.batch212;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Henlo");

        Phonebook phonebook = new Phonebook();
        Contact contactOne = new Contact("Yuji Itadori", "09123456789", "Shibuya Tokyo");
        Contact contactTwo = new Contact("Megumi Fushigoro", "09156789876", "Kyoto Japan");
        Contact contactThree = new Contact("Nobara Kugisaki", "09183219876", "Tokyo Japan");
        Contact contactFour = new Contact("John doe", "09154889980 091238907", "Earth");
        phonebook.setContacts(contactOne);
        phonebook.setContacts(contactTwo);
        phonebook.setContacts(contactThree);
        phonebook.setContacts(contactFour);

        //System.out.println(contactOne);
        //System.out.println(contactTwo);
        //System.out.println(contactThree);

        if(phonebook.getContacts().size() == 0 ){
            System.out.println("Your phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()){
                System.out.println("------------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("currently resides at " + contact.getAddress());

            }
        }

    }
}
