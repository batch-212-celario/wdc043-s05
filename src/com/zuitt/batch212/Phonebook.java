package com.zuitt.batch212;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact>
    contacts = new ArrayList<>();

    public Phonebook(){}
    public Phonebook(ArrayList<Contact>contacts){
        this.contacts = contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }
}
